package com.example.donika.altbeacon.activity;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.ListFragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.donika.altbeacon.R;
import com.example.donika.altbeacon.adapter.AdapterProduct;

import org.json.JSONObject;

/**
 * Created by PRINCE on 3/29/2016.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ProductFragment extends Fragment {

    private JSONObject productjson;
    private AdapterProduct mProductListAdapter=new AdapterProduct(getActivity());
    private AdapterProduct productadapter;

    ListView listproduct;
    Button done;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_fragment, container, false);

        listproduct=(ListView)view.findViewById(R.id.lvproduct);
        productjson=mProductListAdapter.getProduct();
        productadapter = new AdapterProduct(getActivity(),productjson);
        listproduct.setAdapter(productadapter);

        done=(Button)view.findViewById(R.id.btndone);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return view;
    }

}
