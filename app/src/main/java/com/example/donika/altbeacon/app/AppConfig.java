package com.example.donika.altbeacon.app;

import android.util.Log;

import com.example.donika.altbeacon.MainActivity;
import com.example.donika.altbeacon.activity.LoginActivity;

public class AppConfig {
    // Server user login url
    public static String uid=LoginActivity.getUser_id();
    public static String URL_LOGIN = "http://192.168.0.86:3000/api/userlogins/login";
    public static String URL_PRODUCT_LIST="http://192.168.0.86:3000/api/users/getProductList/"+uid;

    // Server user register url
    // public static String URL_REGISTER = "http://192.168.0.102/android_login_api/register.php";
}
