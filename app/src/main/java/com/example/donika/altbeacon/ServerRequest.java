package com.example.donika.altbeacon;

import android.util.Log;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;
import java.util.Set;

/**
 * Created by Donika on 15-Mar-16.
 */
public class ServerRequest {

    public boolean login(String username,String password){

        try{

            MongoClient mongoClient=new MongoClient("localhost",27017);

            DB db = mongoClient.getDB("meanApp-Beacon");

            // Get and print all the collections
//            Set<String> colls = db.getCollectionNames();
//            for (String s : colls)
//                Log.d("Collection Name",s);

//            DBCollection table = db.getCollection("users");
//
//            BasicDBObject searchQuery = new BasicDBObject();
//            searchQuery.put("email_Id", username);
//            searchQuery.put("password",password);
//
//            DBCursor cursor = table.find(searchQuery);
//
//          //  Log.d("Count",""+cursor.itcount());
//
//            if (cursor.hasNext()) {
//                return true;
//            }

            mongoClient.close();
        }
        catch(UnknownHostException ex){
            ex.printStackTrace();
            return false;

        }

        return true;

    }

}
