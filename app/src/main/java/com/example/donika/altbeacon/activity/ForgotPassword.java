package com.example.donika.altbeacon.activity;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.donika.altbeacon.R;
import com.example.donika.altbeacon.helper.SQLiteHandler;

import java.util.HashMap;

public class ForgotPassword extends Activity {

    private TextView txtforgotpassword;

    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        txtforgotpassword=(TextView)findViewById(R.id.tvforgotpass);

        db=new SQLiteHandler(getApplicationContext());

        HashMap<String, String> user = db.getUserDetails();

        String pass = user.get("pass");

        Log.e("Fp","pass");

        txtforgotpassword.setText(pass);
    }
}
