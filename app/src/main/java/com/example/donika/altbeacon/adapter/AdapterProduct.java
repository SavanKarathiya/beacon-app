package com.example.donika.altbeacon.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.donika.altbeacon.MainActivity;
import com.example.donika.altbeacon.R;
import com.example.donika.altbeacon.activity.LoginActivity;
import com.example.donika.altbeacon.app.AppConfig;
import com.example.donika.altbeacon.app.AppController;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by PRINCE on 3/29/2016.
 */
public class AdapterProduct extends BaseAdapter{

        private Activity mactivity;
        private LayoutInflater inflater;

        String Product_Name=null;
        int Product_Stock=0;
        JSONObject productjson;

        public AdapterProduct(Activity mactivity){
            this.mactivity=mactivity;
        }

        public AdapterProduct(Activity mactivity,JSONObject productjson)
        {
            this.mactivity=mactivity;
            this.productjson=productjson;
            inflater = (LayoutInflater) mactivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return 0;
        }
        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            Viewholder v1;
            if(convertView == null)
            {
                convertView = inflater.inflate(R.layout.product_list,null);
                v1 = new Viewholder();
                v1.proname = (TextView) convertView.findViewById(R.id.tvproductname);
                v1.stock = (TextView) convertView.findViewById(R.id.tvproductstock);
                v1.cbpro = (CheckBox) convertView.findViewById(R.id.cbproduct);
                convertView.setTag(v1);
            }
            else
            {
                v1 = (Viewholder) convertView.getTag();

            }

            try {

                final String pro_name=getName((int) productjson.get(String.valueOf(position)));


                v1.proname.setText(getName((int)productjson.get(String.valueOf(position))));
                v1.stock.setText(String.valueOf(getStock((int) productjson.get(String.valueOf(position)))));

                v1.cbpro.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Toast.makeText(mactivity,pro_name + " is selected", Toast.LENGTH_LONG).show();

                    }
                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return convertView;
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        private String getName(int place)
        {
            try {
                JSONObject list = getProduct();
                JSONArray arr = new JSONArray(list);
                JSONObject jobj = arr.getJSONObject(0);
                JSONArray product = jobj.getJSONArray("product_Id");

                JSONObject jsonname = product.getJSONObject(place);
                Product_Name = jsonname.getString("product");

                return Product_Name;
            }
            catch (Exception e){
                e.printStackTrace();
            }
            return Product_Name;
        }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private int getStock(int place)
    {
        try {
            JSONObject list = getProduct();
            JSONArray arr = new JSONArray(list);
            JSONObject jobj = arr.getJSONObject(0);
            JSONArray product = jobj.getJSONArray("product_Id");

            JSONObject jsonstock = product.getJSONObject(place);
            Product_Stock = jsonstock.getInt("stock");

            return Product_Stock;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return Product_Stock;
    }

        public JSONObject getProduct(){

            String tag_string_req = "req_product_list";

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    AppConfig.URL_PRODUCT_LIST,null, new Response.Listener<JSONObject>() {



                @Override
                public void onResponse(JSONObject jsonObject) {

                    Log.d("Main Activity", "Response1: " + jsonObject.toString());

                    try {
                            Log.d("Adapter", "Inside Adapter");

                        productjson=jsonObject;

                    }
                     catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.d("Adapter", "Login Error2: " + error.getMessage());
                }
            })
            {@Override
             public Map < String, String > getHeaders() throws AuthFailureError {
                    HashMap < String, String > headers = new HashMap < String, String > ();
                    String encodedCredentials = Base64.encodeToString("passwordandlogin".getBytes(), Base64.NO_WRAP);
                    headers.put("Authorization", "Basic " + encodedCredentials);

                    return headers;
                }
            };



            ;


            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_string_req);

            return productjson;
        }

        static class Viewholder
        {
            TextView proname;
            TextView stock;
            CheckBox cbpro;
        }
}

