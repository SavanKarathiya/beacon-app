package com.example.donika.altbeacon.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.donika.altbeacon.MainActivity;
import com.example.donika.altbeacon.R;
import com.example.donika.altbeacon.app.AppConfig;
import com.example.donika.altbeacon.app.AppController;
import com.example.donika.altbeacon.helper.SQLiteHandler;
import com.example.donika.altbeacon.helper.SessionManager;

import org.altbeacon.beacon.Beacon;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.String;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends Activity {

    private EditText username,password;
    private TextView forgotPassword;
    private Button login;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    public static String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username=(EditText)findViewById(R.id.etemail);
        password=(EditText)findViewById(R.id.etpass);
        forgotPassword=(TextView)findViewById(R.id.tvforgotpass);
        login=(Button)findViewById(R.id.btnlogin);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        session=new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
//        if (session.isLoggedIn()) {
//            // User is already logged in. Take him to main activity
//            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//            startActivity(intent);
//            finish();
//        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=username.getText().toString().trim();
                String pass=password.getText().toString().trim();

                // Check for empty data in the form
                if (!email.isEmpty() && !pass.isEmpty()) {
                    checkLogin(email, pass);
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Hello", "Forgot Password");
                Intent i = new Intent(getApplicationContext(),
                        ForgotPassword.class);
                startActivity(i);
                finish();
            }
        });
    }

    /**
     * function to verify login details in mysql db
     * */
    private void checkLogin(final String email, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {

                user_id=response.toString();

                Log.d("Login Activity", "Login Response1: " + user_id);
                hideDialog();

                try {
                        //session.setLogin(true);

                        Log.d("Response", "Inside Response");

                        Intent intent = new Intent(LoginActivity.this,
                                MainActivity.class);
                        startActivity(intent);
                        finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("Login activity", "Login Error2: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                      ""+ error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        })


        {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email_Id", email);
                params.put("password", password);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                 params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(stringRequest, tag_string_req);
    }

    public static String getUser_id(){
        //Log.d("USER",user_id);
        return user_id;}

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}