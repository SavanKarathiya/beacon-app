package com.example.donika.altbeacon;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.donika.altbeacon.activity.LoginActivity;
import com.example.donika.altbeacon.activity.ProductFragment;
import com.example.donika.altbeacon.helper.SQLiteHandler;
import com.example.donika.altbeacon.helper.SessionManager;

import java.util.HashMap;

public class MainActivity extends Activity {

    // private SQLiteHandler db;
    private SessionManager session;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        FragmentTransaction f = getFragmentManager().beginTransaction();
        f.replace(R.id.frame1,new ProductFragment());
        f.commit();
    }

    private void logoutUser() {
        session.setLogin(false);

        // db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

}

        // SqLite database handler
        //db = new SQLiteHandler(getApplicationContext());

        // session manager
//        session = new SessionManager(getApplicationContext());
//
//        if (!session.isLoggedIn()) {
//            logoutUser();
//        }

        // Fetching user details from sqlite
        //HashMap<String, String> user = db.getUserDetails();

       // String name = user.get("name");
        //String email = user.get("email");

        // Displaying the user details on the screen
//        txtName.setText("Donika");
//        txtEmail.setText("email");
//
//        // Logout button click event
//        btnLogout.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                logoutUser();
//            }
//        });
//    }
//
//    /**
//     * Logging out the user. Will set isLoggedIn flag to false in shared
//     * preferences Clears the user data from sqlite users table
//     * */
